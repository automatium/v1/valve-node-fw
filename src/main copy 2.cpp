// #define SERIAL_PROTOCOL_VERSION 16

// #include <Arduino.h>
// #include <ArduinoJson.h>
// #include <RHDatagram.h>
// #include <RH_RF95.h>
// #include <Base64.h>

// #if defined(MOTEINO_M0)
// #if defined(SERIAL_PORT_USBVIRTUAL)
// #define Serial SERIAL_PORT_USBVIRTUAL // Required for Serial on Zero based boards
// #endif
// #endif

// RH_RF95 driver(A2, 9);
// RHDatagram manager(driver);
// uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];

// bool init_status = false;

// void blink_bool_result(bool status, int blinks)
// {
//   if (!status)
//   {
//     while (true)
//     {
//       for (int x = 0; x < blinks; x++)
//       {
//         digitalWrite(LED_BUILTIN, HIGH);
//         delay(50);
//         digitalWrite(LED_BUILTIN, LOW);
//         delay(100);
//       }
//       delay(500);
//     }
//   }
// }

// void setup()
// {
//   // serial
//   Serial.begin(115200);

//   // led
//   pinMode(LED_BUILTIN, OUTPUT);

//   // init
//   driver.waitPacketSent();
//   init_status = manager.init();
//   blink_bool_result(init_status, 2);

//   // set frequency
//   blink_bool_result(driver.setFrequency(915.0), 3);

//   // set radio preset
//   blink_bool_result(driver.setModemConfig(RH_RF95::Bw125Cr45Sf128), 4);

//   // set tx power
//   driver.setTxPower(0);

//   // set modem address
//   manager.setThisAddress(2);

//   // now we're ready!
//   digitalWrite(LED_BUILTIN, HIGH);
//   delay(50);
//   digitalWrite(LED_BUILTIN, LOW);
// }

// void loop()
// {
//   DynamicJsonDocument output_doc(JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(7) + 350);

//   uint8_t len = sizeof(buf);
//   uint8_t from;
//   uint8_t to;
//   uint8_t id;
//   uint8_t flags;

//   bool result = manager.recvfrom(buf, &len, &from, &to, &id, &flags);

//   // in this case false return value probably just means no packet
//   // so we return "ok" in either case
//   output_doc["status"] = "Ok";
//   JsonObject packet = output_doc.createNestedObject("body");
//   if (result)
//   {
//     digitalWrite(LED_BUILTIN, HIGH);

//     packet["type"] = "Packet";

//     packet["src"] = from;
//     packet["dst"] = to;
//     packet["len"] = len;
//     packet["id"] = id;
//     packet["flags"] = flags;

//     int encodedLength = Base64.encodedLength((int)len);
//     char encodedString[encodedLength];
//     Base64.encode(encodedString, (char *)buf, (int)len);
//     packet["data"] = encodedString;
//   }
//   else
//   {
//     packet["type"] = "NoPacket";
//   }

//   ATOMIC_BLOCK_START;
//   serializeJson(output_doc, Serial);
//   Serial.println("");
//   ATOMIC_BLOCK_END;

//   delay(1000);
// }