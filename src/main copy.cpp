// #define SERIAL_PROTOCOL_VERSION 16

// #include <Arduino.h>
// #include <ArduinoJson.h>
// #include <RHDatagram.h>
// #include <RH_RF95.h>
// #include <Base64.h>

// #if defined(MOTEINO_M0)
// #if defined(SERIAL_PORT_USBVIRTUAL)
// #define Serial SERIAL_PORT_USBVIRTUAL // Required for Serial on Zero based boards
// #endif
// #endif

// RH_RF95 driver(A2, 9);
// RHDatagram manager(driver);
// uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];

// bool init_status = false;

// void setup()
// {
//   Serial.begin(115200);

//   pinMode(LED_BUILTIN, OUTPUT);
// }

// void print_bool_result(bool condition, const char *err_msg)
// {
//   const size_t capacity = JSON_OBJECT_SIZE(2);
//   DynamicJsonDocument doc(capacity);

//   if (condition)
//   {
//     doc["status"] = "Ok";
//     doc["body"] = nullptr;
//   }
//   else
//   {
//     doc["status"] = "Err";
//     doc["err_msg"] = err_msg;
//   }

//   ATOMIC_BLOCK_START;
//   serializeJson(doc, Serial);
//   Serial.println("");
//   ATOMIC_BLOCK_END;
// }

// void loop()
// {
//   DynamicJsonDocument output_doc(JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(7) + 350);

//   DynamicJsonDocument doc(JSON_OBJECT_SIZE(10) + 100);
//   deserializeJson(doc, Serial);

//   if (doc["type"].as<char *>() == nullptr)
//   {
//     return;
//   }
//   else if (strcmp(doc["type"], "ReadVersion") == 0)
//   {
//     output_doc["status"] = "Ok";
//     output_doc["body"] = SERIAL_PROTOCOL_VERSION;

//     ATOMIC_BLOCK_START;
//     serializeJson(output_doc, Serial);
//     Serial.println("");
//     ATOMIC_BLOCK_END;
//   }
//   else if (strcmp(doc["type"], "Init") == 0)
//   {
//     // init will fail if a packet is still being transmitted
//     driver.waitPacketSent();

//     init_status = manager.init();

//     print_bool_result(init_status, "Error initializing modem");
//   }
//   else if (init_status && strcmp(doc["type"], "SetFrequency") == 0)
//   {
//     float frequency = doc["freq"];

//     print_bool_result(driver.setFrequency(frequency), "Error setting frequency");
//   }
//   else if (init_status && strcmp(doc["type"], "SetRadioPreset") == 0)
//   {
//     const char *preset_str = doc["preset"];
//     RH_RF95::ModemConfigChoice preset;
//     if (strcmp(doc["type"], "Bw125Cr45Sf128") == 0)
//     {
//       preset = RH_RF95::Bw125Cr45Sf128;
//     }
//     else if (strcmp(doc["type"], "Bw500Cr45Sf128") == 0)
//     {
//       preset = RH_RF95::Bw500Cr45Sf128;
//     }
//     else if (strcmp(doc["type"], "Bw31_25Cr48Sf512") == 0)
//     {
//       preset = RH_RF95::Bw31_25Cr48Sf512;
//     }
//     else if (strcmp(doc["type"], "Bw125Cr48Sf4096") == 0)
//     {
//       preset = RH_RF95::Bw125Cr48Sf4096;
//     }

//     print_bool_result(driver.setModemConfig(preset), "Error setting modem config choice");
//   }
//   else if (init_status && strcmp(doc["type"], "SetTxPower") == 0)
//   {
//     int8_t power = doc["power"];
//     driver.setTxPower(power);

//     print_bool_result(true, "");
//   }
//   else if (init_status && strcmp(doc["type"], "SetModemAddress") == 0)
//   {
//     uint8_t address = doc["address"];
//     manager.setThisAddress(address);

//     output_doc["status"] = "Ok";
//     output_doc["body"] = manager.thisAddress();

//     ATOMIC_BLOCK_START;
//     serializeJson(output_doc, Serial);
//     Serial.println("");
//     ATOMIC_BLOCK_END;
//   }
//   else if (init_status && strcmp(doc["type"], "Tx") == 0)
//   {
//     uint8_t dst = doc["dst"];
//     const char *input_msg = doc["msg"];

//     char encoded_msg[strlen(input_msg) + 1];
//     strcpy(encoded_msg, input_msg);

//     int decoded_length = Base64.decodedLength(encoded_msg, strlen(encoded_msg));
//     char decoded_bytes[decoded_length];
//     Base64.decode(decoded_bytes, encoded_msg, strlen(encoded_msg));

//     bool result = manager.sendto((uint8_t *)decoded_bytes, decoded_length, dst);
//     print_bool_result(result, "Error sending packet");

//     // // now re-encode and echo back for redundancy
//     // int encoded_length = Base64.encodedLength(decoded_length);
//     // char encoded_string[encoded_length];
//     // Base64.encode(encoded_string, decoded_bytes, decoded_length);

//     // output_doc["status"] = "Ok";
//     // output_doc.set("body", (const char *)encoded_string);

//     // serializeJson(output_doc, Serial);
//     // Serial.println("");
//   }
//   else if (init_status && strcmp(doc["type"], "Rx") == 0)
//   {
//     uint8_t len = sizeof(buf);
//     uint8_t from;
//     uint8_t to;
//     uint8_t id;
//     uint8_t flags;

//     bool result = manager.recvfrom(buf, &len, &from, &to, &id, &flags);

//     // in this case false return value probably just means no packet
//     // so we return "ok" in either case
//     output_doc["status"] = "Ok";
//     JsonObject packet = output_doc.createNestedObject("body");
//     if (result)
//     {
//       packet["type"] = "Packet";

//       packet["src"] = from;
//       packet["dst"] = to;
//       packet["len"] = len;
//       packet["id"] = id;
//       packet["flags"] = flags;

//       int encodedLength = Base64.encodedLength((int)len);
//       char encodedString[encodedLength];
//       Base64.encode(encodedString, (char *)buf, (int)len);
//       packet["data"] = encodedString;
//       digitalWrite(LED_BUILTIN, HIGH);
//     }
//     else
//     {
//       packet["type"] = "NoPacket";
//     }

//     ATOMIC_BLOCK_START;
//     serializeJson(output_doc, Serial);
//     Serial.println("");
//     ATOMIC_BLOCK_END;
//   }
//   else
//   {
//     print_bool_result(false, "Method not found");
//   }
// }