// rf95_reliable_datagram_client.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple addressed, reliable messaging client
// with the RHReliableDatagram class, using the RH_RF95 driver to control a RF95 radio.
// It is designed to work with the other example rf95_reliable_datagram_server
// Tested with Anarduino MiniWirelessLoRa, Rocket Scream Mini Ultra Pro with the RFM95W

#include <RHDatagram.h>
#include <RH_RF95.h>

#define CLIENT_ADDRESS 1
#define SERVER_ADDRESS 2

// dip toes
// #define VIN_MEASURE A4
// #define EN_9V 3
// #define AIN1 4
// #define AIN2 5
// #define BIN1 6
// #define BIN2 7

// center slice
#define VIN_MEASURE A4
#define EN_DRV 4
#define AIN1 5
#define AIN2 6
#define BIN1 7
#define BIN2 A0

int valves[2][2] = {
    {AIN2, AIN1},
    {BIN2, BIN1}};
bool valve_status[2] = {false, false};
unsigned long start_times[2] = {0, 0};

// Singleton instance of the radio driver
// use the second line with the Moteino M0
RH_RF95 driver;
// RH_RF95 driver(A2, 9);

// Class to manage message delivery and receipt, using the driver declared above
RHDatagram manager(driver, CLIENT_ADDRESS);

// Need this on Arduino Zero with SerialUSB port (eg RocketScream Mini Ultra Pro)
// #define Serial SerialUSB

void blink_bool_result(bool status, int blinks)
{
  if (!status)
  {
    while (true)
    {
      for (int x = 0; x < blinks; x++)
      {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(50);
        digitalWrite(LED_BUILTIN, LOW);
        delay(100);
      }
      delay(500);
    }
  }
}

// #define BOOST_WARMUP 6000 // dip toes
#define BOOST_WARMUP 1000 // center slice
#define PULSE_LENGTH 100
unsigned long last_switch_time = 0;

// the valve will not actually switch unless the boost converter has had time to recover
// clients are expected to retry, or set wait_for_warmup = true
void switchValve(uint8_t valve, bool state, bool wait_for_warmup = false, bool switch_anyway = false)
{
  // switch the valve, only if it's not already in the correct state
  if (switch_anyway || valve_status[valve] != state)
  {
    // wait_for_warmup specifies whether we should block or return if the rest period between switching has passed
    if (wait_for_warmup)
    {
      while (millis() - last_switch_time < BOOST_WARMUP)
      {
        // wait
      }
    }
    else if (millis() - last_switch_time < BOOST_WARMUP)
    {
      // return immediately
      return;
    }

    int pinToPulse = valves[valve][state];

    // enable the driver
    digitalWrite(EN_DRV, HIGH);
    
    // pulse the pin
    digitalWrite(pinToPulse, HIGH);
    delay(PULSE_LENGTH);
    digitalWrite(pinToPulse, LOW);

    // disable the driver
    digitalWrite(EN_DRV, LOW);

    valve_status[valve] = state;
    last_switch_time = millis();
  }

  start_times[valve] = millis();
}

void setup()
{
  // I'm alive!
  digitalWrite(LED_BUILTIN, HIGH);

  // serial
  Serial.begin(115200);

  // pins
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(EN_DRV, OUTPUT);
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);

  // init radio
  driver.waitPacketSent();
  blink_bool_result(manager.init(), 2);

  // set frequency
  blink_bool_result(driver.setFrequency(915.0), 3);

  // set radio preset
  blink_bool_result(driver.setModemConfig(RH_RF95::Bw125Cr45Sf128), 4);

  // set tx power
  driver.setTxPower(20);

  // set modem address
  // manager.setThisAddress(CLIENT_ADDRESS);

  // set up ADC
  pinMode(A5, INPUT);

  // make sure all valves are off
  switchValve(0, 0, true, true);
  switchValve(1, 0, true, true);

  // now we're ready!
  digitalWrite(LED_BUILTIN, LOW);
}

// Dont put this on the stack:
uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];

void sendStatus(uint8_t to)
{
  uint32_t vin = analogRead(VIN_MEASURE);

  // re-use the rx buffer
  snprintf((char *)buf, sizeof(buf), "<s1|%u%u|%lu>", valve_status[0], valve_status[1], vin);

  // send off the message, minus the null terminator added by snprintf
  manager.sendto(buf, strlen((char *)buf), to);
}

void maybeTurnOff(uint8_t valve)
{
  if (valve_status[valve] == true)
  {
    if ((millis() - start_times[valve]) > 60000)
    {
      switchValve(valve, false);
    }
  }
}

void loop()
{
  // turn off, if needed
  maybeTurnOff(0);
  maybeTurnOff(1);

  // get a new packet
  uint8_t len = sizeof(buf);
  uint8_t from;
  uint8_t to;
  uint8_t id;
  uint8_t flags;

  bool result = manager.recvfrom(buf, &len, &from, &to, &id, &flags);

  if (result)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    if (len == 3 && buf[0] == 'V')
    {
      // port
      uint8_t port = buf[1] - '0';
      // state
      uint8_t state = buf[2] - '0';
      switchValve(port, state);

      sendStatus(from);
    }
    else if (len == 1 && buf[0] == 'S')
    {
      sendStatus(from);
    }
    digitalWrite(LED_BUILTIN, LOW);
  }
}