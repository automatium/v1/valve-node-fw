// #include <LowPower.h>
#include <RH_RF95.h>

// Shared settings ==============

// I have 915MHz Moteinos
#define FREQ 915.0

// fastest TX and shortest range
#define MODEM_PRESET RH_RF95::Bw125Cr45Sf128

// Transmit settings  ==============

// std preamble
// #define TX_PREAMBLE 8

// how long to send a barrage of packets
// #define TX_TIME 6500

// how long to wait for an ACK
// #define ACK_TIMEOUT 10000

// Recieve settings ==============

// short preamble for ACKs (the transmitter never sleeps)
// #define ACK_PREAMBLE 8

// how long to wait for a packet to come in
// #define RX_DELAY SLEEP_15MS

// how long to wait before sending an ACK
// #define ACK_DELAY SLEEP_8S

// Main sleep period
// #define LONG_SLEEP 8000
// #define LONG_SLEEP SLEEP_8S
